module AstroLib

  # A class to perform calendrical conversions.
  # @author Mark Huss (original version)
  # @author Jan Lindblom (Ruby version) <janlindblom@fastmail.fm>
  class DateOps
    # Abbreviated month names
    month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

    # Pseudo-enum for calendar type
    GREGORIAN = 0
    JULIAN = 1

    # Converts a day/month/year to a Julian day in +long+ form.
    def self.dmy_to_day(day, month, year, calendar=GREGORIAN)

    end
  end

  private

  E_JULIAN_GREGORIAN = 1721060

  # A support class for DateOps
  class DateConversionData

  end

end