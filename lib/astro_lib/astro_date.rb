require "astro_lib/astro"

module AstroLib
  # A support class for DateOps.
  #
  # This class stores a date/time value at a precision of one second.
  # This date/time can be specified using either the Gregorian Calendar
  # (by default) or the Julian Calendar.
  #
  # It also supports conversions to and from the Julian Day number.
  class AstroDate

    # @!attribute [rw] day
    #   @return [Integer] day of the month
    attr_accessor :day

    # @!attribute [rw] month
    #   @return [Integer] month of the year
    attr_accessor :month

    # @!attribute [rw] year
    #   @return [Integer] year
    attr_accessor :year

    # @!attribute [rw] seconds
    #   @return [Integer] Seconds past midnight == day fraction.
    #   Valid values range from 0 to {Astro::SECONDS_PER_DAY} - 1.
    attr_accessor :seconds

    # Create a new instance of AstroDate.
    #
    # @option opts [Integer] :day Day of the month (1..31)
    # @option opts [Integer] :month Month of the year (1..12)
    # @option opts [Integer] :year Year
    # @option opts [Integer] :hour Hour of the day (0..23)
    # @option opts [Integer] :min Minute of the hour (0..59)
    # @option opts [Integer] :sec Second of the minute (0..59)
    # @option opts [Integer] :seconds Time in seconds past midnight. This must be in the range from +0+ to {Astro::SECONDS_PER_DAY} - 1.
    def initialize(opts=nil)
      # Default constructor = epoch J2000 (noon on 1/1/2000)
      self.day = 1
      self.month = 1
      self.year = 2000
      self.seconds = 12 * Astro::SECONDS_PER_HOUR

      unless opts.nil?
        # Literal (member by member) constructor
        self.day = opts[:day] if opts.has_key? :day
        self.month = opts[:month] if opts.has_key? :month
        self.year = opts[:year] if opts.has_key? :year
        self.seconds = opts[:seconds] if opts.has_key? :seconds

        # Explicit day, month, year, hour, minute, and second constructor
        if opts.has_key? :min and opts.has_key? :hour
          self.seconds = opts[:hour] * Astro::SECONDS_PER_HOUR + opts[:min] * Astro::SECONDS_PER_MINUTE + opts[:sec]
        end
      end
    end

    # Literal (member by member) constructor
    #
    # @param [Integer] day Day of the month (1...31)
    # @param [Integer] month Month of the year (1..12)
    # @param [Integer] year Year
    # @param [Integer] seconds Time in seconds past midnight. This must be in the range from +0+ to {Astro::SECONDS_PER_DAY} - 1.
    # @return [AstroDate] a new instance of +AstroDate+.
    def self.constructor_literal(day, month, year, seconds)
      ad = AstroDate.new
      ad.day = day
      ad.month = month
      ad.year = year
      ad.seconds = seconds
      ad
    end

    # Explicit day, month, year, hour, minute, and second constructor
    #
    # @param [Integer] day Day of the month (1...31)
    # @param [Integer] month Month of the year (1..12)
    # @param [Integer] year Year
    # @param [Integer] hour Hour of the day (0...23)
    # @param [Integer] min Minute of the hour (0...59)
    # @param [Integer] sec Second of the minute (0...59)
    # @return [AstroDate] a new instance of +AstroDate+.
    def self.constructor_explicit(day, month, year, hour, min, sec)
      ad = AstroDate.new
      ad.day = day
      ad.month = month
      ad.year = year
      ad.seconds = hour * Astro::SECONDS_PER_HOUR + min * Astro::SECONDS_PER_MINUTE + sec
      ad
    end

    # Day, Month, Year constructor (time defaults to 00:00:00 = midnight)
    #
    # @param [Integer] day Day of the month (1...31)
    # @param [Integer] month Month of the year (1..12)
    # @param [Integer] year Year
    # @return [AstroDate] a new instance.
    def self.constructor_day_month_year(day, month, year)
      ad = AstroDate.new
      ad.day = day
      ad.month = month
      ad.year = year
      ad.seconds = 0
      ad
    end

    # Day, Month, Year + fraction of a day constructor
    #
    # @param [Integer] day Day of the month (1...31)
    # @param [Integer] month Month of the year (1..12)
    # @param [Integer] year Year
    # @param [Float] day_fraction Fraction of the day. This must be greater than or equal to +0.0+ and less than +1.0+.
    # @return [AstroDate] a new instance.
    def self.constructor_day_month_year_dayfraction(day, month, year, day_fraction)
      ad = AstroDate.new
      ad.day = day
      ad.month = month
      ad.year = year
      ad.seconds = day_fraction * Astro::SECONDS_PER_DAY
      ad
    end

    # Julian Day constructor.
    #
    # @param [Float] jd Julian day number
    # @return [AstroDate] a new instance.
    def self.constructor_julian_day(jd)
      ad = AstroDate.new
      # The conversion formulas and magic numbers are from Meeus, Chapter 7.
      jd = jd + 0.5 # Einf�gung Strickling
      zz = jd.floor
      ff = jd - zz
      aa = zz;
      if zz >= 2299161.0
        a = ((zz - 1867216.25) / Astro::TO_CENTURIES).to_i
        aa += 1 + a - a/4
      end
      bb = aa + 1524
      cc = ((bb-122.1)/365.25).to_i
      dd = (cc * 365.25).to_i
      ee = ((bb-dd)/30.6001).to_i

      exact_day = ff + bb - dd - (30.6001*ee).to_i
      ad.day = exact_day.to_i
      ad.month = ee < 14 ? ee-1 : ee-13
      ad.year = cc - 4715
      ad.year = ad.year - 1 if ad.month > 2

      ad.seconds = (exact_day - ad.day) * Astro::SECONDS_PER_DAY

      ad
    end

    # FUNCTIONS

    # Convert an AstroDate to a Julian Day.
    #
    # @param [AstroDate] ad The date to convert
    # @param [Boolean] julian +true+ = Julian calendar, else Gregorian
    # @return [Float] The Julian Day that corresponds to the specified AstroDate
    def self.jd(ad, julian=false)
      # The conversion formulas and magic numbers are from Meeus, Chapter 7.

      dd = ad.day
      mm = ad.month
      yy = ad.year
      if mm < 3
        yy--
        mm += 12
      end
      aa = yy/100
      bb = julian ? 0 : 2 - aa + aa/4

      day_fraction = ad.seconds / Astro::SECONDS_PER_DAY;

      return day_fraction + (365.25 * (yy + 4716)).to_i + (30.6001 * (mm + 1)).to_i + dd + bb - 1524.5
    end

    # Convert this instance of AstroDate to a Julian Day.
    #
    # @param [Boolean] julian +true+ = Julian calendar, else Gregorian
    # @return [Float] The Julian Day that corresponds to this AstroDate instance
    def jd(julian=false)
      AstroDate.jd(self, julian)
    end

    # accessors - NOTE: These do no sanity checking

    # Get the Hour.
    #
    # This function truncates, and does not round up to nearest hour.
    # For example, this function will return '1' at all times from
    # 01:00:00 to 01:59:59 inclusive.
    #
    # @return [Integer] The hour of the day for this instance of AstroDate, not rounded
    def hour
      (seconds / Astro::SECONDS_PER_HOUR).to_i
    end

    # Get the rounded hour.
    #
    # Returns the hour of the day rounded to nearest hour.
    # For example, this function will return '1' at times 01:00:00 to
    # 01:29:59, and '2' at times 01:30:00 to 01:59:59.
    #
    # @return [Integer] The hour of the day for this instance of AstroDate, rounded to the nearest hour.
    def hour_round
      ((seconds / Astro::SECONDS_PER_HOUR) + Astro::ROUND_UP).to_i
    end

    # Get the minute.
    #
    # This function truncates, and does not round up to nearest minute.
    # For example, this function will return 20 at all times from
    # 1:20:00 to 1:20:59 inclusive.
    #
    # @return [Integer] The minute of the hour for this instance of AstroDate, not rounded.
    def minute
      ((seconds - (hour * Astro::SECONDS_PER_HOUR)) / Astro::SECONDS_PER_MINUTE).to_i
    end

    # Get the rounded minute.
    #
    # Returns the minute of the hour for this instance of AstroDate,
    # rounded to nearest minute.
    # For example, this function will return 20 at times 1:20:00 to
    # 1:20:29, and 21 at times 1:20:30 to 1:20:59.
    #
    # @return [Integer] The minute of the hour for this instance of AstroDate,
    # rounded to the nearest minute.
    def minute_round
      (((seconds - (hour * Astro::SECONDS_PER_HOUR)) / Astro::SECONDS_PER_MINUTE) + Astro::ROUND_UP).to_i
    end

    # Get the second.
    #
    # @return [Integer] The second of the minute for this instance of AstroDate.
    def second
      (seconds - (hour * Astro::SECONDS_PER_HOUR) - (minute * Astro::SECONDS_PER_MINUTE)).to_i
    end

    # Convert this AstroDate instance to a String, formatted to the minute.
    # This function rounds the exact time to the nearest minute.
    #
    # The format of the returned string is +YYYY-MM-DD hh:mm+.
    #
    # @return [String] A formatted date/time String
    def to_min_string
      year + '-' + month + '-' + day + ' ' + hour + ':' + minute_round
    end
  end
end